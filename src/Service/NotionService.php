<?php

namespace App\Service;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class NotionService
{
    private $client;
    private $param;

    public function __construct(HttpClientInterface $client, ParameterBagInterface $param)
    {
        $this->client = $client;
        $this->param = $param;
    }

    public function makeRequest(?array $body = null): array
    {
        $response = $this->client->request(
            'POST',
            'https://api.notion.com/v1/databases/'.$this->param->get('NOTION_DATABASE').'/query',
            [
                'auth_bearer' => $this->param->get('NOTION_API'),
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Notion-Version' => '2021-05-11',
                ],
                'json' => $body,
            ]
        );
        return $response->toArray()['results'];
    }

    // Récupère toutes les données de notion
    public function getAll(): array
    {
        return $this->formatData($this->makeRequest());
    }

    // Récupère les données de notion pour une entrée
    public function getOne(string $id): array
    {
        $array = [
            'filter' => 
                [
                    'property' => 'id',
                    'text' => [
                        'equals' => $id,
                    ],
                ],
            ];

        return $this->formatData($this->makeRequest($array));
    }

    // Format les données
    public function formatData(array $data): array
    {
        $formattedData = [];

        foreach ($data as $value) {
            $formattedData[] = [
                'id' => $value['properties']['id']['formula']['string'],
                'nom' => $value['properties']['nom']['title'][0]['plain_text'],
                'infos' => $value['properties']['infos']['rich_text'][0]['plain_text'] ,
                'tags' => $value['properties']['tags']['rich_text'][0]['plain_text'] ,
                'tags_1' => $value['properties']['tags_1']['rich_text'][0]['plain_text'] ,
                'detail' => $value['properties']['detail']['rich_text'][0]['plain_text'] ,
                'image' => $value['properties']['image']['files'][0]['file']['url'] ?? $value['properties']['Image']['files'][0]['file']['url'] = null,
                'image_p' => $value['properties']['image_p']['files'][0]['file']['url'] ?? $value['properties']['Image']['files'][0]['file']['url'] = null,
                'lien' => $value['properties']['lien']['url'],
                'lien_github' => $value['properties']['lien_github']['url'],
            ];
        }

        return $formattedData;
    }

}